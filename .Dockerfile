FROM node:latest

RUN apt-get update -y

RUN mkdir /usr/bin/app

WORKDIR /usr/bin/app

COPY ./package.json .

RUN npm install

CMD [ "npm", "start" ]
