const frmSubmitJob = document.querySelector('form');
const taJobDefinitionObjectJson = document.querySelector('#ta-job-definition-object-json');
const btnCreateJob = document.querySelector('#btn-create-job');
const divJsonParsingError = document.querySelector('#div-json-parsing-error');

const jsonIsValid = jsonString => {
  try {
    return jsonString.startsWith('{') && jsonString.endsWith('}') && JSON.parse(jsonString) ? true : false;
  } catch (error) {
    console.error(error);
    return false;
  }
}

taJobDefinitionObjectJson.addEventListener('input', async ev => {
  btnCreateJob[`${jsonIsValid(ev.target.value.trim()) ? 'remove' : 'set'}Attribute`]('disabled', 'disabled');
});

frmSubmitJob.addEventListener('submit', async ev => {
  ev.preventDefault();
  try {
    taJobDefinitionObjectJson.value = taJobDefinitionObjectJson.value.trim();
    console.log(taJobDefinitionObjectJson.value);
    const response = await fetch(frmSubmitJob.getAttribute('action'), {
      headers: {
        'Content-Type': 'application/json'
      },
      method: frmSubmitJob.getAttribute('method'),
      body: taJobDefinitionObjectJson.value
    });
    
    console.log(await response.json());

  } catch (error) {
    console.error('error while parsing json')
  }
});