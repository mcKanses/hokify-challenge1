require('dotenv').config();

const axios = require('axios');
const bodyParser = require('body-parser');
const express = require('express');
const _ = require('lodash');

const getAccessTokenMiddleware = require(`${process.cwd()}/express-middleware/getAccessToken.mw`);

const app = express();

const PORT = 3333 || process.env.PORT;

app.use(bodyParser.json());

app.use(express.static(`${process.cwd()}/public`));

app.use(async (err, req, res, next) => {
  if (err.message.includes('JSON')) return res.status(400).send('json data malformed');
  res.status(400).send('an error occured');
});

app.post('/createJob', getAccessTokenMiddleware, async (req, res, next) => {
  try {
    const jobData = req.body;

    console.log(jobData);

    const response = await axios.put(`https://test.hokify.com/ats-api/job/createOrUpdateJob`, {
      // sourceId: process.env.MANDATOR_ID,
      sourceId: "6",
      params: req.body,
      lang: 'de'
    }, {
      headers: {
        Authorization: `Bearer ${res.locals.accessToken.access_token}`
      }
    });


    res.send(response.data);
  } catch (error) {
    console.log(error.message);
    next(error);
  }
});

app.get('/test', getAccessTokenMiddleware, async (req, res, next) => {
  try {
    res.send(res.locals.accessToken);
  } catch (error) {
    next(error);
  }
});

app.listen(PORT, () => console.log(`server listening on port ${PORT}`));