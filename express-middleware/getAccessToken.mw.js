const axios = require('axios');

const { MANDATOR_ID, MANDATOR_TOKEN } = process.env;

const getAccessTokenMiddleware = (req, res, locals) => (async (req, res, next) => {
  try {
    res.locals.accessToken = btoa(btoa(`${MANDATOR_ID}:${MANDATOR_TOKEN}`));

    const response = await axios.post(`https://test.hokify.com/ats-api/auth/token`, null, {
      headers: {
        // accept: 'application/json',
        'Authorization': `Basic ${btoa(`${MANDATOR_ID}:${MANDATOR_TOKEN}`)}`
      }
    });
    
    res.locals.accessToken = response?.data;
    next();
  } catch (error) {
    console.error(error.response);
    next(error);
  }
})(req, res, locals);

module.exports = getAccessTokenMiddleware;
